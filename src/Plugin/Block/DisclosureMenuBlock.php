<?php

namespace Drupal\disclosure_menu\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\MenuActiveTrailInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\system\Plugin\Block\SystemMenuBlock;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a "Disclosure Menu" block.
 *
 * @Block(
 *   id = "disclosure_menu",
 *   admin_label = @Translation("Disclosure menu"),
 *   category = @Translation("Disclosure Menu"),
 *   deriver = "Drupal\system\Plugin\Derivative\SystemMenuBlock"
 * )
 */
class DisclosureMenuBlock extends SystemMenuBlock {

  /**
   * The active menu trail service.
   *
   * @var \Drupal\Core\Menu\MenuActiveTrailInterface
   */
  protected $menuActiveTrail;

  /**
   * Constructs a new DisclosureMenuBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menu_tree
   *   The menu tree service.
   * @param \Drupal\Core\Menu\MenuActiveTrailInterface $menu_active_trail
   *   The active menu trail service.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, MenuLinkTreeInterface $menu_tree, MenuActiveTrailInterface $menu_active_trail) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $menu_tree, $menu_active_trail);
    $this->menuActiveTrail = $menu_active_trail;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('menu.link_tree'),
      $container->get('menu.active_trail'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'submenu_disclosure_levels' => -1,
      'disclosure_button_include_chevron' => 1,
      'disclosure_button_include_label' => 0,
      'disclosure_button_label' => 'More [menu-link:title] pages',
      'menu_disclosure' => 0,
      'menu_disclosure_button_include_label' => 1,
      'menu_disclosure_button_label' => 'Open [menu:name]',
      'include_default_js' => 1,
      'include_hover_js' => 0,
      'resolve_hover_click' => 'keyboard_only',
      'hover_show_delay' => 150,
      'hover_hide_delay' => 250,
      'include_css' => 'horizontal',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $menu_depth = ':input[name="settings[depth]"]';

    $form['submenu_disclosure'] = [
      '#title' => $this->t('Submenu disclosure settings'),
      '#type' => 'details',
      '#states' => [
        'invisible' => [
          $menu_depth => ['value' => '1'],
        ],
      ],
      '#process' => [[self::class, 'processMenuLevelParents']],
    ];

    $form['submenu_disclosure']['submenu_disclosure_levels'] = [
      '#title' => $this->t('Maximum disclosure levels'),
      '#type' => 'select',
      '#options' => [
        -1 => $this->t('Unlimited'),
        0 => $this->t('None'),
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
        6 => 6,
        7 => 7,
        8 => 8,
        9 => 9,
      ],
      '#description' => $this->t(
        'How many menu levels should include disclosure buttons for their submenus?'
      ),
      '#default_value' => $this->configuration['submenu_disclosure_levels'],
    ];
    $submenu_disclosure_levels = ':input[name="settings[submenu_disclosure_levels]"]';

    $form['submenu_disclosure']['disclosure_button_include_chevron'] = [
      '#title' => $this->t('Insert chevron icon in submenu disclosure buttons?'),
      '#type' => 'radios',
      '#options' => [
        0 => $this->t('No'),
        1 => $this->t('Yes'),
      ],
      '#default_value' => $this->configuration['disclosure_button_include_chevron'],
      '#states' => [
        'visible' => [
          $submenu_disclosure_levels => ['!value' => 0],
        ],
      ],
    ];

    $form['submenu_disclosure']['disclosure_button_include_label'] = [
      '#title' => $this->t('Submenu disclosure button labels'),
      '#type' => 'radios',
      '#options' => [
        0 => $this->t('None'),
        1 => $this->t('Custom text'),
      ],
      '#default_value' => $this->configuration['disclosure_button_include_label'],
      '#states' => [
        'visible' => [
          $submenu_disclosure_levels => ['!value' => 0],
        ],
      ],
    ];
    $disclosure_button_include_label = ':input[name="settings[disclosure_button_include_label]"]';

    $form['submenu_disclosure']['disclosure_button_label_container'] = [
      '#type' => 'container',
      '#process' => [[self::class, 'processMenuLevelParents']],
      'disclosure_button_label' => [
        '#type' => 'textfield',
        '#size' => 60,
        '#maxlength' => 128,
        '#default_value' => $this->configuration['disclosure_button_label'],
        '#states' => [
          'required' => [
            $menu_depth => ['!value' => 1],
            $submenu_disclosure_levels => ['!value' => 0],
            $disclosure_button_include_label => ['value' => 1],
          ],
        ],
      ],
      'disclosure_button_label_token_helper' => [
        '#theme' => 'token_tree_link',
        '#token_types' => ['menu-link'],
      ],
      '#states' => [
        'visible' => [
          $submenu_disclosure_levels => ['!value' => 0],
          $disclosure_button_include_label => ['value' => 1],
        ],
      ],
    ];

    $form['menu_disclosure'] = [
      '#title' => $this->t('Full menu disclosure settings'),
      '#type' => 'details',
      '#process' => [[self::class, 'processMenuLevelParents']],
    ];

    $form['menu_disclosure']['menu_disclosure'] = [
      '#title' => $this->t('Add an additional disclosure button to toggle the entire menu?'),
      '#type' => 'radios',
      '#options' => [
        0 => $this->t('No'),
        1 => $this->t('Yes'),
      ],
      '#default_value' => $this->configuration['menu_disclosure'],
    ];
    $menu_disclosure = ':input[name="settings[menu_disclosure]"]';

    $form['menu_disclosure']['menu_disclosure_button_include_label'] = [
      '#title' => $this->t('Full menu disclosure button label'),
      '#type' => 'radios',
      '#options' => [
        0 => $this->t('None'),
        1 => $this->t('Custom text'),
      ],
      '#default_value' => $this->configuration['menu_disclosure_button_include_label'],
      '#states' => [
        'visible' => [
          $menu_disclosure => ['value' => 1],
        ],
      ],
    ];
    $menu_disclosure_button_include_label = ':input[name="settings[menu_disclosure_button_include_label]"]';

    $form['menu_disclosure']['menu_disclosure_button_label_container'] = [
      '#type' => 'container',
      '#process' => [[self::class, 'processMenuLevelParents']],
      'menu_disclosure_button_label' => [
        '#type' => 'textfield',
        '#size' => 60,
        '#maxlength' => 128,
        '#default_value' => $this->configuration['menu_disclosure_button_label'],
        '#states' => [
          'required' => [
            $menu_disclosure => ['value' => 1],
            $menu_disclosure_button_include_label  => ['value' => 1],
          ],
        ],
      ],
      'menu_disclosure_button_label_token_helper' => [
        '#theme' => 'token_tree_link',
      ],
      '#states' => [
        'visible' => [
          $menu_disclosure => ['value' => 1],
          $menu_disclosure_button_include_label  => ['value' => 1],
        ],
      ],
    ];

    $form['disclosure_js'] = [
      '#title' => $this->t('Javascript settings'),
      '#type' => 'details',
      '#process' => [[self::class, 'processMenuLevelParents']],
    ];

    $form['disclosure_js']['include_default_js'] = [
      '#type' => 'radios',
      '#title' => $this->t('Include default Javascript'),
      '#description' => $this->t(
        'This will set disclosure buttons to toggle open submenus.'
      ),
      '#default_value' => $this->configuration['include_default_js'],
      '#options' => [
        1 => $this->t('Yes'),
        0 => $this->t('No'),
      ],
    ];

    $form['disclosure_js']['include_hover_js'] = [
      '#type' => 'radios',
      '#title' => $this->t('Include hover navigation Javascript'),
      '#description' => $this->t(
        'Open submenus when hovering over the parent menu item.'
      ),
      '#default_value' => $this->configuration['include_hover_js'],
      '#options' => [
        1 => $this->t('Yes'),
        0 => $this->t('No'),
      ],
      '#states' => [
        'visible' => [
          ':input[name="settings[include_default_js]"]' => ['value' => 1],
        ],
      ],
    ];

    $visible_on_all_js_enabled = [
      'visible' => [
        ':input[name="settings[disclosure_js][include_default_js]"]' => ['value' => 1],
        ':input[name="settings[disclosure_js][include_hover_js]"]' => ['value' => 1],
      ],
    ];

    $form['disclosure_js']['resolve_hover_click'] = [
      '#title' => $this->t('Resolve disclosure button hover and click'),
      '#description' => $this->t(
        "With hovering enabled, the disclosure button will toggle the submenu
        both on hover and click. This means that clicking while hovering over
        the button will close the submenu by toggling it twice. Here are options
        to resolve this conflict."
      ),
      '#type' => 'radios',
      '#default_value' => $this->configuration['resolve_hover_click'],
      '#options' => [
        'keyboard_only' => $this->t(
          'Ignore pointer click on disclosure button, so it can only be triggered by keyboard'
        ),
        'only_open' => $this->t(
          'Only open the submenu when clicking the disclosure button, never toggle the submenu closed'
        ),
        'no_change' => $this->t('Do not change the behavior'),
      ],
      '#states' => $visible_on_all_js_enabled,
    ];

    $form['disclosure_js']['hover_show_delay'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Hover show delay'),
      '#description' => $this->t(
        'How many milliseconds to wait before displaying the submenu when hovering over the parent menu item'
      ),
      '#default_value' => $this->configuration['hover_show_delay'],
      '#states' => $visible_on_all_js_enabled,
    ];

    $form['disclosure_js']['hover_hide_delay'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Hover hide delay'),
      '#description' => $this->t(
        'How many milliseconds to wait before hiding the submenu when the pointer is no longer hovering over the parent menu item or the submenu'
      ),
      '#default_value' => $this->configuration['hover_hide_delay'],
      '#states' => $visible_on_all_js_enabled,
    ];

    $form['styles'] = [
      '#title' => $this->t('Style settings'),
      '#type' => 'details',
      '#open' => true,
      '#process' => [[self::class, 'processMenuLevelParents']],
    ];

    $form['styles']['include_css'] = [
      '#type' => 'radios',
      '#title' => $this->t('Default menu styles'),
      '#default_value' => $this->configuration['include_css'],
      '#options' => [
        0 => $this->t('No default styles'),
        'horizontal' => $this->t('Horizontal menu'),
        'vertical' => $this->t('Vertical menu'),
      ],
      'horizontal' => [
        '#description' => $this->t(
          'Style the menu as a horizontal dropdown menu.'
        ),
      ],
      'vertical' => [
        '#description' => $this->t(
          'Style the menu as a vertical dropdown menu.'
        ),
      ]
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->configuration['submenu_disclosure_levels'] = $form_state
      ->getValue('submenu_disclosure_levels');
    $this->configuration['disclosure_button_include_chevron'] = $form_state
      ->getValue('disclosure_button_include_chevron');
    $this->configuration['disclosure_button_include_label'] = $form_state
      ->getValue('disclosure_button_include_label');
    $this->configuration['disclosure_button_label'] = $form_state
      ->getValue('disclosure_button_label');
    $this->configuration['menu_disclosure'] = $form_state
      ->getValue('menu_disclosure');
    $this->configuration['menu_disclosure_button_include_label'] = $form_state
      ->getValue('menu_disclosure_button_include_label');
    $this->configuration['menu_disclosure_button_label'] = $form_state
      ->getValue('menu_disclosure_button_label');
    $this->configuration['include_default_js'] = $form_state
      ->getValue('include_default_js');
    $this->configuration['include_hover_js'] = $form_state
      ->getValue('include_hover_js');
    $this->configuration['resolve_hover_click'] = $form_state
      ->getValue('resolve_hover_click');
    $this->configuration['hover_show_delay'] = $form_state
      ->getValue('hover_show_delay');
    $this->configuration['hover_hide_delay'] = $form_state
      ->getValue('hover_hide_delay');
    $this->configuration['include_css'] = $form_state
      ->getValue('include_css');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = parent::build();
    $build['#theme'] = 'menu__disclosure';

    $id = HTML::getUniqueId('disclosure-menu');

    if ($this->configuration['include_default_js']) {
      $build['#attached']['library'][] = 'disclosure_menu/menu';
      $build['#attached']['drupalSettings']['disclosureMenu'][$id]['id'] = $id;
      if ($this->configuration['include_hover_js']) {
        $build['#menu_attributes']['class'][] = 'hover';
        $build['#attached']['library'][] = 'disclosure_menu/style-hover';
        $build['#attached']['drupalSettings']['disclosureMenu'][$id]['hover'] = 1;
        $build['#attached']['drupalSettings']['disclosureMenu'][$id]['resolveHoverClick'] = $this->configuration['resolve_hover_click'];
        $build['#attached']['drupalSettings']['disclosureMenu'][$id]['hoverShowDelay'] = $this->configuration['hover_show_delay'];
        $build['#attached']['drupalSettings']['disclosureMenu'][$id]['hoverHideDelay'] = $this->configuration['hover_hide_delay'];
      }
    }

    if ($this->configuration['depth'] !== 1) {
      $build['#submenu_disclosure_levels'] = $this->configuration['submenu_disclosure_levels'];
      $build['#disclosure_button_include_chevron'] = $this->configuration['disclosure_button_include_chevron'];
      $build['#disclosure_button_include_label'] = $this->configuration['disclosure_button_include_label'];
      $build['#disclosure_button_label'] = $this->configuration['disclosure_button_label'];
    }

    $build['#menu_disclosure'] = $this->configuration['menu_disclosure'];
    if ($this->configuration['menu_disclosure']) {
      $build['#menu_disclosure_button_include_label'] = $this->configuration['menu_disclosure_button_include_label'];
      $build['#menu_disclosure_button_label'] = $this->configuration['menu_disclosure_button_label'];
    }

    if ($style = $this->configuration['include_css']) {
      if ($style == 'horizontal') {
        $build['#menu_attributes']['class'][] = 'horizontal';
        $build['#attached']['library'][] = 'disclosure_menu/style-horizontal';
      }
      if ($style == 'vertical') {
        $build['#menu_attributes']['class'][] = 'vertical';
        $build['#attached']['library'][] = 'disclosure_menu/style-vertical';
      }
    }

    $build['#id'] = $id;

    return $build;
  }

}
